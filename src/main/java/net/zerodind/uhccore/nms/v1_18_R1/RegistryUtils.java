package net.zerodind.uhccore.nms.v1_18_R1;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_18_R1.CraftServer;

import net.minecraft.core.MappedRegistry;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.WritableRegistry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.dedicated.DedicatedServer;
import net.zerodind.uhccore.nms.NmsOperationException;

/**
 * Utility class for registry operations.
 */
public abstract class RegistryUtils {

	private static RegistryAccess getRegistryAccess() {
		final DedicatedServer server = ((CraftServer) Bukkit.getServer()).getServer();
		return server.registryAccess();
	}

	/**
	 * Obtains write access to a given registry and runs an operation on it.
	 *
	 * @param <T> the type of registry
	 * @param registryKey the key for the registry
	 * @param writeOperation the operation to run with write access
	 *
	 * @throws NmsOperationException if the operation fails
	 */
	public static <T> void write(ResourceKey<Registry<T>> registryKey,
			WriteOperation<T> writeOperation) throws NmsOperationException {
		final MappedRegistry<T> registry = (MappedRegistry<T>) getRegistryAccess().ownedRegistry(registryKey)
			.orElseThrow(() -> new NmsOperationException("Missing registry: " + registryKey));
		writeOperation.run(registry);
	}

	@FunctionalInterface
	public interface WriteOperation<T> {
		void run(WritableRegistry<T> t) throws NmsOperationException;
	}

}
